import{createWriteStream} from 'fs'
import { browser, by, element, ExpectedConditions} from 'protractor'

describe('Protractor Typescript Demo', function() {
	browser.ignoreSynchronization = true; // for non-angular websites
	// it('get Cookie test in Protractor', function() {
	// 	browser.get("https://google.com")
	// 	// take screenshot
	// 	browser.takeScreenshot().then(function (png) {
	// 		var stream = createWriteStream("exceptionzzz.png");
	// 		stream.write(new Buffer(png, 'base64'));
	// 		stream.end();
	// 	});
	// });
	it('https site for testing', function() {
		browser.get("http://aavtrain.com/")
		// take screenshot
		browser.takeScreenshot().then(function (png) {
			var stream = createWriteStream("exceptionasd.png");
			stream.write(new Buffer(png, 'base64'));
			stream.end();
		});
	});
});